package com.feedit.server.config.ouath_security;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.token.TokenStore;

@Configuration
@EnableResourceServer
public class OAauth2ResourceServerConfig extends ResourceServerConfigurerAdapter {

    private static final String REST_API_ANT_PATTERN = "/v1/**";
    private static final String ADMIN = "ADMIN";
    private static final String USER = "USER";
    private static final String[] ALL = {ADMIN, USER};

    private final TokenStore tokenStore;

    @Autowired
    public OAauth2ResourceServerConfig(TokenStore tokenStore) {
        this.tokenStore = tokenStore;
    }


    @Override
    public void configure(ResourceServerSecurityConfigurer resources) throws Exception {
        resources.resourceId("feedit-api").tokenStore(this.tokenStore);
    }

    @Override
    public void configure(HttpSecurity http) throws Exception {
        http.csrf().disable().exceptionHandling()
                .and()
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and()
                .authorizeRequests()
                .antMatchers(HttpMethod.OPTIONS).permitAll()
                .antMatchers("/v1/admin/**").hasRole(ADMIN)
                .antMatchers(REST_API_ANT_PATTERN).authenticated()
                .and()
                .logout();
    }
}
