package com.feedit.server.config.user_detail_service;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;

import java.util.Collection;

public class UserDetail extends User{

    private String[] roles;
    private final String str_roles;

    public UserDetail(String username, String password, Collection<? extends GrantedAuthority> authorities) {
        super(username, password, authorities);
        StringBuilder builder=new StringBuilder();

        roles=  authorities.stream().map(authority->{
            String role=authority.getAuthority();
            builder.append(role);
            builder.append(" ");
            return role;
        }).toArray(String[]::new);

        str_roles=builder.toString();
    }

    public String getRoles() {
        return str_roles;
    }
}
