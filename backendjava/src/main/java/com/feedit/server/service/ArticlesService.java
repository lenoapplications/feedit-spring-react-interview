package com.feedit.server.service;

import com.feedit.server.model.Articles;

import java.util.Collection;

public interface ArticlesService  {

    Collection<Articles> findAllArticlesByUserId(Long id);
}
