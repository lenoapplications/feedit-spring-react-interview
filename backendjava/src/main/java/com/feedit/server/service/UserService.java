package com.feedit.server.service;

import com.feedit.server.model.User;

public interface UserService {

    User getUserByUsername(String username);
}
