package com.feedit.server.service.impl;

import com.feedit.server.model.Articles;
import com.feedit.server.repository.user.ArticlesRepository;
import com.feedit.server.service.ArticlesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;

@Service
public class ArticlesServiceImpl implements ArticlesService {

    @Autowired
    ArticlesRepository articlesRepository;


    @Override
    public Collection<Articles> findAllArticlesByUserId(Long id) {
        return articlesRepository.findAllByUsername_id(id);
    }
}
