package com.feedit.server.controller.v1.admin;


import com.feedit.server.model.Articles;
import com.feedit.server.service.ArticlesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;

@RequestMapping(value = "v1/admin/")
@RestController
public class ArticlesController {


    @Autowired
    private ArticlesService articlesService;


    @RequestMapping(value = "/getAllById",method = RequestMethod.POST)
    public Collection<Articles> getAllArticles(@RequestParam Long id){
        System.out.println(id);

        return articlesService.findAllArticlesByUserId(id);
    }


}
