package com.feedit.server.repository.user;


import com.feedit.server.model.Articles;
import com.feedit.server.model.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Collection;

@Repository
public interface ArticlesRepository extends JpaRepository<Articles,Long> {


    @Query(value = "SELECT * FROM Articles  WHERE username_id = ?1",nativeQuery = true)
    Collection<Articles> findAllByUsername_id(Long id);

}




