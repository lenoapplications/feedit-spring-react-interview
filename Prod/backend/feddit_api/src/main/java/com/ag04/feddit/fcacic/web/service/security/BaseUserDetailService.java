package com.ag04.feddit.fcacic.web.service.security;


import com.ag04.feddit.fcacic.model.User;
import com.ag04.feddit.fcacic.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class BaseUserDetailService implements UserDetailsService {

    @Autowired
    private UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        User user=userRepository.findByUsername(username);

        UserDetails userDetails = new org.springframework.security.core.userdetails.User(
                username, user.getPassword(),
                user.isEnabled(),
                true, true, true,
                user.getRoles() );

        return userDetails;
    }
}
