package com.ag04.feddit.fcacic.web.validator.article_validator;


import com.ag04.feddit.fcacic.web.domain.dto.article.ArticleDtoDelete;
import com.ag04.feddit.fcacic.web.exceptions.errors_codes.articles_error_codes.ArticlesErrorCodes;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import java.util.List;

@Component
public class ArticleDeleteValidator implements Validator {

    private final ArticlesErrorCodes FIELD_NULL=ArticlesErrorCodes.NULL;
    private final ArticlesErrorCodes FIELD_EMPTY=ArticlesErrorCodes.EMPTY;


    @Override
    public boolean supports(Class<?> clazz) {
        return ArticleDtoDelete.class.equals(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        ArticleDtoDelete articleDtoDelete=(ArticleDtoDelete) target;

        checkField(articleDtoDelete.getIdArticles(),errors);
    }

    private void checkField(List<Long> listOfId,Errors errors){
        String errorMsg=null;

        if (listOfId == null){
            errorMsg=FIELD_NULL.getError("idArticles");
        }
        else if (listOfId.size() == 0){
            errorMsg=FIELD_EMPTY.getError("idArticles");
        }
        if (errorMsg != null){
            errors.rejectValue("idArticles",errorMsg);
        }
    }
}
