package com.ag04.feddit.fcacic.web.mapper.dto.article_dto;


import com.ag04.common.mapper.EntityMapper;
import com.ag04.feddit.fcacic.model.article.Article;
import com.ag04.feddit.fcacic.web.domain.dto.article.ArticleDto;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

@Component
public class ArticleDtoMapper implements EntityMapper<Article,ArticleDto> {
    private static final String ARTICLE_DTO_KEY = "articleDto";
    private static final String ARTICLE_KEY="article";

    @Override
    public ArticleDto mapFromEntity(Article article, Locale locale) {
        Map<String,Object> config=new HashMap<>();
        config.put(ARTICLE_DTO_KEY,new ArticleDto());
        return mapFromEntity(article,locale,config);
    }

    @Override
    public ArticleDto mapFromEntity(Article article, Locale locale, Map<String, Object> config) {
        ArticleDto articleDto=(ArticleDto) config.get(ARTICLE_DTO_KEY);

        articleDto.setId(article.getId());
        articleDto.setHeadline(article.getHeadline());
        articleDto.setAuthor(article.getAuthor());
        articleDto.setLink(article.getLink());
        articleDto.setNumberOfVotes(article.getVotes());

        return articleDto;
    }

    @Override
    public Article mapToEntity(ArticleDto source, Locale locale) {
        Map<String,Object> config=new HashMap<>();
        config.put(ARTICLE_KEY,new Article());

        return mapToEntity(source,locale,config);
    }

    @Override
    public Article mapToEntity(ArticleDto source, Locale locale, Map<String, Object> config) {

        Article article=(Article) config.get(ARTICLE_KEY);

        article.setHeadline(source.getHeadline());
        article.setAuthor(source.getAuthor());
        article.setLink(source.getLink());

        return article;
    }

    public ArticleDto mapFromEntity(Article article){
        return mapFromEntity(article,null);
    }

    public Article mapToEntity(ArticleDto articleDto){
        return mapToEntity(articleDto,null);
    }
}
