package com.ag04.feddit.fcacic.web.annotations.aspects.response.articles_resp;

import com.ag04.feddit.fcacic.model.article.Article;
import com.ag04.feddit.fcacic.web.domain.dto.resp.Response;
import com.ag04.feddit.fcacic.web.domain.resp_codes.resp_articles.RespArticlesCodes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class ArticlesResponse {

    @Autowired
    private Response response;


    public ResponseEntity<Response> createResponse(String code, Object returnValue){

        switch(code){
            case "Article created":return articleCreated((Article)returnValue);

            case "Articles deleted":return articlesDeleted((List<Article>)returnValue);

            default:return null;
        }
    }


    private ResponseEntity<Response> articleCreated(Article article){
        String status=String.format(RespArticlesCodes.CREATE_ARTICLE_OK.toString(),article.getCreatedAt());
        response.setStatus(status);
        return new ResponseEntity<Response>(response, HttpStatus.OK);
    }

    private ResponseEntity<Response> articlesDeleted(List<Article> articlesDeleted){
        int size=articlesDeleted.size();
        String extension = (size > 1) ? "es" : "e";
        String status=String.format(RespArticlesCodes.DELETE_ARTICLE_OK.toString(),size,extension);
        String content=response.listToString(articlesDeleted);

        response.setStatus(status);
        response.setContent(content);

        return new ResponseEntity<Response>(response,HttpStatus.OK);
    }
}
