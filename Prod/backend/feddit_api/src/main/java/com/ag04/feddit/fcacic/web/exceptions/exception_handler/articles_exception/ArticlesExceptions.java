package com.ag04.feddit.fcacic.web.exceptions.exception_handler.articles_exception;

import com.ag04.feddit.fcacic.exceptions.articles_exceptions.ValidationArticleFailed;
import com.ag04.feddit.fcacic.web.domain.dto.resp.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;


@ControllerAdvice
public class ArticlesExceptions extends ResponseEntityExceptionHandler {

    @Autowired
    private Response response;


    @ExceptionHandler(ValidationArticleFailed.class)
    public ResponseEntity<Response> articleValidField(ValidationArticleFailed validationException){
        String status=validationException.getMessage();
        response.setStatus(status);
        return new ResponseEntity<Response>(response,HttpStatus.OK);
    }

}
