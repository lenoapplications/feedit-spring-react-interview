package com.ag04.feddit.fcacic.web.validator.article_validator;

import com.ag04.feddit.fcacic.web.domain.dto.article.ArticleDto;
import com.ag04.feddit.fcacic.web.exceptions.errors_codes.articles_error_codes.ArticlesErrorCodes;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

@Component
public class ArticleCreateValidator implements Validator {

    private final ArticlesErrorCodes FIELD_NULL=ArticlesErrorCodes.NULL;
    private final ArticlesErrorCodes FIELD_EMPTY=ArticlesErrorCodes.EMPTY;

    @Override
    public boolean supports(Class<?> clazz) {
        return ArticleDto.class.equals(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {

        ArticleDto articleForm=(ArticleDto) target;

        checkField("headline",articleForm.getHeadline(),errors);
        checkField("author",articleForm.getAuthor(),errors);
        checkField("link",articleForm.getLink(),errors);
    }


    private void checkField(String field,String value,Errors errors){
        String errorMsg=null;

        if (value == null){
            errorMsg=FIELD_NULL.getError(field);
        }
        else if (value.isEmpty()){
            errorMsg=FIELD_EMPTY.getError(field);
        }
        if (errorMsg != null){
            errors.rejectValue(field,errorMsg);
        }
    }
}
