package com.ag04.feddit.fcacic.web.domain.resp_codes.resp_articles;

public enum RespArticlesCodes {

    CREATE_ARTICLE_OK("article created - %s"),
    DELETE_ARTICLE_OK("deleted %s articl%s")
    ;
    private final String text;

    RespArticlesCodes(final String text) {
        this.text = text;
    }

    @Override
    public String toString() {
        return text;
    }



}
