package com.ag04.feddit.fcacic.web.controller.v1.articlesController;



import com.ag04.feddit.fcacic.annotations.interfaces.response.BasicResponse;
import com.ag04.feddit.fcacic.service.impl.ArticleServiceImpl;
import com.ag04.feddit.fcacic.web.domain.dto.article.ArticleDto;
import com.ag04.feddit.fcacic.exceptions.articles_exceptions.ValidationArticleFailed;
import com.ag04.feddit.fcacic.web.domain.dto.article.ArticleDtoDelete;
import com.ag04.feddit.fcacic.web.domain.dto.resp.Response;
import com.ag04.feddit.fcacic.web.mapper.dto.article_dto.ArticleDtoMapper;
import com.ag04.feddit.fcacic.web.validator.article_validator.ArticleCreateValidator;
import com.ag04.feddit.fcacic.web.validator.article_validator.ArticleDeleteValidator;
import com.ag04.support.web.RequestParamUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;


@RestController
@RequestMapping(value = "/v1/articles")
public class ArticleController {
    private final String controller="Articles";

    private final ArticleServiceImpl articleServiceimpl;
    private final ArticleDtoMapper articleDtoMapper;

    private final ArticleCreateValidator articleCreateValidator;
    private final ArticleDeleteValidator articleDeleteValidator;


    @Autowired
    public ArticleController(ArticleServiceImpl articleServiceimpl,
                             ArticleDtoMapper articleDtoMapper,
                             ArticleCreateValidator articleFormValidator,
                             ArticleDeleteValidator articleDeleteValidator,
                             Response response) {
        this.articleServiceimpl=articleServiceimpl;
        this.articleDtoMapper=articleDtoMapper;
        this.articleCreateValidator=articleFormValidator;
        this.articleDeleteValidator=articleDeleteValidator;
    }




    @GetMapping(value = "/")
    public Page<ArticleDto> getArticles(@RequestParam(value="page")int page,
                                     @RequestParam(value="size")int pageSize,
                                     @RequestParam(value="sort")String sort){
        Pageable pageable= RequestParamUtils.pageable(page,pageSize,sort);
        return articleServiceimpl.getAllArticlesWithQueryParams(pageable).map(article->articleDtoMapper.mapFromEntity(article));


    }



    @PostMapping(value="/")
    @BasicResponse(controller = controller, code = "Article created")
    public Object addNewArticle(@Valid @RequestBody final ArticleDto articleDto, BindingResult bindingResult)
            throws ValidationArticleFailed {

        String username=(String) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        if (bindingResult.hasErrors()){
            throw new ValidationArticleFailed(bindingResult);
        }
        return articleServiceimpl.createArticle(articleDtoMapper.mapToEntity(articleDto,null),username);

    }

    @DeleteMapping(value="/")
    @BasicResponse(controller = controller ,code = "Articles deleted")
    public Object deleteArticle(@Valid @RequestBody final ArticleDtoDelete articleDtoDelete,BindingResult bindingResult)
            throws ValidationArticleFailed{

        if (bindingResult.hasErrors()){
            throw new ValidationArticleFailed(bindingResult);
        }
        return articleServiceimpl.deleteArticles(articleDtoDelete.getIdArticles());
    }




    @InitBinder("articleDtoDelete")
    private void initArticleDeleteValidator(WebDataBinder binder){
        binder.setValidator(articleDeleteValidator);
    }

    @InitBinder("articleDto")
    private void initArticleFormValidator(WebDataBinder binder){
        binder.setValidator(articleCreateValidator);
    }
}
