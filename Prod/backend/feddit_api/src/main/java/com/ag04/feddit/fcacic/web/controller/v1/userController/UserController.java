package com.ag04.feddit.fcacic.web.controller.v1.userController;


import com.ag04.feddit.fcacic.model.User;
import com.ag04.feddit.fcacic.service.impl.UserServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequestMapping(value = "/v1/admin/")
@RestController
public class UserController {

    @Autowired
    UserServiceImpl userService;

    @GetMapping(value = "/getadmin")
    public User getAdmin(){
        System.out.println("Tu sam");
       return userService.findByUsername("admin");
    }
}
