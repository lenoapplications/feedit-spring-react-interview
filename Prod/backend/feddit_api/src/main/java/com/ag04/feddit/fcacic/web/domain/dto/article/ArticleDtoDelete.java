package com.ag04.feddit.fcacic.web.domain.dto.article;

import java.io.Serializable;
import java.util.List;

public class ArticleDtoDelete implements Serializable{

    private List<Long> idArticles;

    public List<Long> getIdArticles() {
        return idArticles;
    }

    public void setIdArticles(List<Long> idArticles) {
        this.idArticles = idArticles;
    }

}
