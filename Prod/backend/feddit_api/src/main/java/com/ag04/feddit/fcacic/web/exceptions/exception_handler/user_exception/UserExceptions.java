package com.ag04.feddit.fcacic.web.exceptions.exception_handler.user_exception;


import com.ag04.common.exception.EntityNotFoundException;
import com.ag04.feddit.fcacic.web.domain.dto.resp.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class UserExceptions {

    @Autowired
    private Response response;

    @ExceptionHandler(EntityNotFoundException.class)
    public ResponseEntity<Response> entityNotFound(EntityNotFoundException entityException){
        String status=entityException.getMessage();
        response.setStatus(status);
        return new ResponseEntity<Response>(response, HttpStatus.OK);
    }
}
