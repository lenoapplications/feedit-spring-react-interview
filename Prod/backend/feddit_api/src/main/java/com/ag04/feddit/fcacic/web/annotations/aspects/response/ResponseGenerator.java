package com.ag04.feddit.fcacic.web.annotations.aspects.response;


import com.ag04.feddit.fcacic.model.article.Article;
import com.ag04.feddit.fcacic.annotations.interfaces.response.BasicResponse;
import com.ag04.feddit.fcacic.web.annotations.aspects.response.articles_resp.ArticlesResponse;
import com.ag04.feddit.fcacic.web.domain.dto.resp.Response;
import com.ag04.feddit.fcacic.web.domain.resp_codes.resp_articles.RespArticlesCodes;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import java.lang.reflect.Method;


@Aspect
@Component
public class ResponseGenerator {


    private Response response;
    private ArticlesResponse articlesResp;

    @Autowired
    public ResponseGenerator(Response response,ArticlesResponse articlesResp){
        this.response=response;
        this.articlesResp=articlesResp;
    }




    @Around("@annotation(com.ag04.feddit.fcacic.annotations.interfaces.response.BasicResponse)")
    private Object responseGenerator(ProceedingJoinPoint joinPoint) throws Throwable{

        Method method=((MethodSignature)joinPoint.getSignature()).getMethod();
        BasicResponse respGen=(BasicResponse)
                method.getAnnotation(BasicResponse.class);

        Object returnValue =joinPoint.proceed();

        return checkController(respGen.controller(),respGen.code(),returnValue);
    }


    private ResponseEntity<Response> checkController(String controller,String code,Object returnValue){
        if (controller.equals("Articles")){
            return articlesResp.createResponse(code,returnValue);
        }
        return new ResponseEntity<Response>(HttpStatus.BAD_REQUEST);
    }


    private ResponseEntity<Response> defaultResponse(){
        return new ResponseEntity<Response>(HttpStatus.OK);
    }

}
