package com.ag04.feddit.fcacic.web.exceptions.errors_codes.articles_error_codes;


public enum ArticlesErrorCodes {

    NULL("'%s' field not provided"),
    EMPTY("no information was put '%s' in field"),
    ;
    private final String text;

    ArticlesErrorCodes(final String text) {
        this.text = text;
    }


    @Override
    public String toString() {
        return text;
    }

    public String getError(String field){
       return String.format(text,field);
    }


}
