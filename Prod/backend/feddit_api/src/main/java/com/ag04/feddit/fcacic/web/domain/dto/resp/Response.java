package com.ag04.feddit.fcacic.web.domain.dto.resp;


import com.ag04.feddit.fcacic.utils.ResponseBuilder;
import com.fasterxml.jackson.annotation.JsonInclude;
import org.springframework.stereotype.Component;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Component
public class Response extends ResponseBuilder {

    private String status;
    private Object content;


    public Response(){
        super();
    }

    public Object getContent() {
        Object contentTemp=this.content;
        this.content=null;
        return contentTemp;
    }

    public void setContent(Object response) {
        this.content = response;
    }

    public String getStatus() { return status; }

    public void setStatus(String status) {
        this.status = status;
    }

}
