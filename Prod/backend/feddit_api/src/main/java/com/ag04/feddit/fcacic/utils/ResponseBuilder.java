package com.ag04.feddit.fcacic.utils;

import com.ag04.feddit.fcacic.model.article.Article;
import java.util.List;

public class ResponseBuilder {


    public String listToString(List<Article> listOfObjects){
        if(listOfObjects.size()==0)
            return "";

        StringBuilder builder=new StringBuilder();

        listOfObjects.forEach(article -> {builder.append(article.getHeadline());builder.append(" , ");});

        return builder.substring(0,builder.length()-2);
    }
}
