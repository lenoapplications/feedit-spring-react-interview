package com.ag04.feddit.fcacic.model.article;


import com.ag04.feddit.fcacic.model.entity_listener.article.ArticleAuditableEntity;
import com.fasterxml.jackson.annotation.JsonInclude;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "Articles")
public class Article extends ArticleAuditableEntity implements Serializable {


    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "USERNAME_ID")
    private Long username_id;

    @Column(name="HEADLINE")
    private String headline;

    @Column(name="AUTHOR")
    private String author;

    @Column(name="VOTES")
    private Integer votes;

    @Column(name="LINK")
    private String link;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getUsername_id() {
        return username_id;
    }

    public void setUsername_id(Long username_id) {
        this.username_id = username_id;
    }

    public String getHeadline() {
        return headline;
    }

    public void setHeadline(String headline) {
        this.headline = headline;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public Integer getVotes() {
        return votes;
    }

    public void setVotes(Integer votes) {
        this.votes = votes;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }


}
