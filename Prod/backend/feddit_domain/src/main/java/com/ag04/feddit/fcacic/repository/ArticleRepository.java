package com.ag04.feddit.fcacic.repository;

import com.ag04.feddit.fcacic.model.article.Article;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface ArticleRepository extends JpaRepository<Article,Long> {


    @Override
    void delete(Iterable<? extends Article> entities);
}
