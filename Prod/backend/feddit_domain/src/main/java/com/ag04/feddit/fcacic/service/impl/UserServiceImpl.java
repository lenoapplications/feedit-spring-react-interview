package com.ag04.feddit.fcacic.service.impl;


import com.ag04.feddit.fcacic.model.User;
import com.ag04.feddit.fcacic.repository.UserRepository;
import com.ag04.feddit.fcacic.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    UserRepository userRepository;

    @Override
    public User findByUsername(String username) {
        return userRepository.findByUsername(username);
    }

    @Override
    public Boolean existsByUsername(String username) {
        return userRepository.existsByUsername(username);
    }
}
