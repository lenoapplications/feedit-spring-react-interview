package com.ag04.feddit.fcacic.service.impl;

import com.ag04.feddit.fcacic.model.User;
import com.ag04.feddit.fcacic.model.article.Article;
import com.ag04.feddit.fcacic.repository.ArticleRepository;
import com.ag04.feddit.fcacic.repository.UserRepository;
import com.ag04.feddit.fcacic.service.ArticleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.List;


@Service
public class ArticleServiceImpl implements ArticleService{

    @Autowired
    ArticleRepository articleRepository;

    @Autowired
    private UserRepository userRepository;

    @Override
    public Page<Article> getAllArticlesWithQueryParams(Pageable pageable) {

        return articleRepository.findAll(pageable);
    }


    @Override
    public Article createArticle(Article article, String username) {
        Boolean exitUser=userRepository.existsByUsername(username);

        if (!exitUser){
            throw new EntityNotFoundException(username);
        }

        User user =userRepository.findByUsername(username);

        article.setUsername_id(user.getId());
        article.setCreatedBy(user);
        article.setModifiedBy(user);
        article.setVotes(0);

        return articleRepository.save(article);
    }

    @Override
    public List<Article> deleteArticles(List<Long> idArticles) {
        List<Article> articles=articleRepository.findAll(idArticles);
        articleRepository.delete(articles);
        return articles;
    }
}
