package com.ag04.feddit.fcacic.service;

import com.ag04.feddit.fcacic.model.User;

public interface UserService {

    User findByUsername(String username);
    Boolean existsByUsername (String username);

}
