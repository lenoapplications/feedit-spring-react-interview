package com.ag04.feddit.fcacic.exceptions.articles_exceptions;

import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;

public class ValidationArticleFailed extends Exception {


    public ValidationArticleFailed(BindingResult bindingResult){
        super(getErrors(bindingResult));
    }


    private static String getErrors(BindingResult bindingResult){
        StringBuilder builder =new StringBuilder();

        for (ObjectError error:bindingResult.getAllErrors()){
                builder.append(error.getCode());
                builder.append(" , ");
        }
        return builder.substring(0,builder.length()-2);
    }

}
