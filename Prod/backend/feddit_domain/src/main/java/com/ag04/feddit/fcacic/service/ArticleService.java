package com.ag04.feddit.fcacic.service;

import com.ag04.feddit.fcacic.model.article.Article;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;


public interface ArticleService  {

    Page<Article> getAllArticlesWithQueryParams(Pageable pageable);

    Article createArticle(Article article,String username);

    List<Article> deleteArticles(List<Long> idArticles);
}
