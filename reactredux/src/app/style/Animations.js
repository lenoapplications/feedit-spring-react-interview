export function animationHeader(){
    return `
    @keyframes animationHeader {

        0% {
            letter-spacing: -20px;
            color:green;
        }
        50%
        {
            letter-spacing: 10px;
            color:gray;
        }
        100%{
            letter-spacing: 3px;
        }
    }`
}

export function animationLoginResponseMessage(){
    return `
    @keyframes responseMessage{
        0%{
            color: #79090e
        }
        50%{
            color:red;
        }
        100%{
            color: #ff3005;
        }
    } 
    `
}


export function animationOpacityArticles(){

    return`
    @keyframes opacityArticles{
        
        0%{
            opacity:0;
        }
        50%{
            opacity:0.5;
        }
        75%{
            opacity:0.75;
        }
        80%{
            opacity:0.8;
        }
        
        90%{
            opacity:0.9;
        }
        
        95%{
            opacity:0.95;
        }
        98%{
            opacity:0.98;
        }
        100%{
            opacity:1;
        }
    }
    `
}

export function animationDragOutSearch(){

    return `
     @keyframes dragOutSearch{

        0%{
            margin-left:-80%;
            border-right:8px solid black;
            background-color:silver
        }
        100%{
            margin-left:0%;
            border:2px solid rgba(0,0,0,0.05);
        }
    } 
    `
}






export default {animationHeader,animationLoginResponseMessage}