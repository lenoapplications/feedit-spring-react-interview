import React,{Component} from 'react';
import styled from 'styled-components';
import { connect } from 'react-redux';
import {bindActionCreators} from 'redux';
import {gridDisplay} from '../../../../style/GeneralStyle';
import {maxWidth860,minWidth861} from "../../../../style/ResponsiveHandler";
import {animationOpacityArticles} from "../../../../style/Animations";

//Components
import Sidebar from './sidebar/Sidebar'
import Content from './content/Content'


//Components
const MainDiv=styled.div
    `
    ${minWidth861(
        
        `${gridDisplay("8fr 1fr","1fr")}
         grid-template-areas:"articles menu";
        `
    )}
    ${maxWidth860(
        `
         ${gridDisplay("1fr","8fr 1fr")}
         grid-template-areas:"articles"
                             "menu"    
        `
    )}
    height:100%;
    `


class Home extends Component{

    constructor(props){
        super(props);
    }
    render(){
        return (

            <MainDiv>
                <Content/>

                <Sidebar/>
            </MainDiv>

        );
    }
}



function mapStateToProps(store){
    return{
    }
}
function mapDispatchToProps(dispatch){

    return{
    }
}


export default connect(mapStateToProps,mapDispatchToProps)(Home)