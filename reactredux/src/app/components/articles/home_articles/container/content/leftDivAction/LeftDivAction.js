import styled from "styled-components"
import React,{Component} from 'react';
import CSSTransitionGroup from 'react-addons-css-transition-group'
import {connect} from "react-redux";
import {minWidth861} from "../../../../../../style/ResponsiveHandler";
import {animationOpacityArticles,animationDragOutSearch} from "../../../../../../style/Animations";
import {gridDisplay} from "../../../../../../style/GeneralStyle";

const MainDiv=styled.div
    `
    ${animationOpacityArticles()};
    ${minWidth861(
        `
        `
    )}
    grid-area:leftActions;
    animation-name:opacityArticles;
    animation-duration:0.5s;
    `

const SearchDiv=styled.div
    `
    ${animationDragOutSearch()}
    ${minWidth861(
        `
        display:flex;
        flex-direction:column;
        align-items:center;
        padding-top:2%;
        width:90%;
        `
    )}
    margin-left:-85%;
    &:hover{
         margin-left:0;
         animation-name:dragOutSearch;
         animation-duration:0.7s;
         border-radius:2%;
         border:2px solid rgba(0,0,0,0.05);
         background-color:white;
    }
    transition: all 0.7s ease-in-out;
    border:2px solid rgba(0,0,0,0.05);
    border-radius:40%;
    border-right:10px solid green;
    background-color:gray;
    `

const SearchArticle=styled.input
    `
    width:80%;
    color:black;
    font-weight:bold;
    `
const SearchButton=styled.input
    `
    width:50%;
    `



class LeftDivAction extends Component{

    render(){
        return(

            <MainDiv id="test" >
                <SearchDiv>

                    <SearchArticle type="text" onFocus={()=>{console.log("king kong")}}/>
                    <SearchButton type="button" value="SEARCH" onClick={(event)=>{this.props.searchArticle(event,this.props.articles)}}/>

                </SearchDiv>
            </MainDiv>
        )
    }
}


function mapStateToProps(store){
    return{

    }
}
function mapDispatchToProps(dispatch){

    return{
    }
}


export default connect(mapStateToProps,mapDispatchToProps)(LeftDivAction)