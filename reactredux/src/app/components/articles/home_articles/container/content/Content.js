import styled from "styled-components"
import React,{Component} from 'react';
import {connect} from "react-redux";
import {minWidth861} from "../../../../../style/ResponsiveHandler";
import {animationOpacityArticles} from "../../../../../style/Animations";
import {gridDisplay} from "../../../../../style/GeneralStyle";

//Components
import LeftDivAction from './leftDivAction/LeftDivAction'
import RightDivAction from './rightDivAction/RightDivAction'
import TopDivAction from './topDivAction/TopDivAction'
import ListArticles from './listArticles/ListArticles'


const MainDiv=styled.div
    `
    grid-area:articles;
    `


const Articles=styled.div
    `
    ${minWidth861(
        `
         ${gridDisplay("2fr 3fr 1.5fr","1.5fr 7fr 1fr")}
         grid-template-areas:
         "leftActions topActions rightActions"
         "previewMark Articles rightActions"
         "previewMark Articles .";
        `
    )}
    height:100%;
    `


const PreviewMark=styled.div
    `
    ${animationOpacityArticles()};

    grid-area:previewMark;
    animation-name:opacityArticles;
    animation-duration:1.2s;
    `




class Content extends Component {


    render() {
        return (
            <MainDiv>
                <Articles>
                    <TopDivAction/>

                    <LeftDivAction/>

                    <ListArticles/>

                    <RightDivAction/>

                    <PreviewMark/>

                </Articles>

            </MainDiv>
        );
    }

}





function mapStateToProps(store){
    return{

    }
}
function mapDispatchToProps(dispatch){

    return{
    }
}




export default connect(mapStateToProps,mapDispatchToProps)(Content)