import styled from "styled-components"
import React,{Component} from 'react';
import {connect} from "react-redux";
import {minWidth861} from "../../../../../../style/ResponsiveHandler";
import {animationOpacityArticles} from "../../../../../../style/Animations";
import CSSTransitionGroup from 'react-addons-css-transition-group'
import {gridDisplay} from "../../../../../../style/GeneralStyle";




const MainDiv=styled.div
    `
    ${animationOpacityArticles()};
    display:flex;
    
    grid-area:topActions;
    animation-name:opacityArticles;
    animation-duration:1s;
    `





class TopDivAction extends Component{

    render(){
        return(

            <MainDiv>

                <CSSTransitionGroup transitionName="example" transitionEnterTimeout={500} transitionLeaveTimeout={300} >

                    <p>King kong</p>
                </CSSTransitionGroup>

            </MainDiv>
        );
    }
}




function mapStateToProps(store){
    return{

    }
}
function mapDispatchToProps(dispatch){

    return{
    }
}


export default connect(mapStateToProps,mapDispatchToProps)(TopDivAction)