import styled from "styled-components"
import React,{Component} from 'react';
import {connect} from "react-redux";
import {minWidth861} from "../../../../../../style/ResponsiveHandler";
import CSSTransitionGroup from 'react-addons-css-transition-group'
import {animationOpacityArticles} from "../../../../../../style/Animations";
import {gridDisplay} from "../../../../../../style/GeneralStyle"

//Actions
import {initGetArticles} from "../../../actions/ArticleActions";


const MainDiv=styled.div
    `
    ${animationOpacityArticles()};
    grid-area:Articles;
    animation-name:opacityArticles;
    animation-duration:2s;
    `
const Article=styled.div
    `
     ${minWidth861(
        `${gridDisplay("5% 10% 85%","1fr 1fr")}
          grid-template-areas:
          "vote voteup headline"
          "vote votedown author"
         `
    )}
    overflow:hidden;
    border:2px solid rgba(0,0,0,0.25);
    margin-bottom:2%;
    border-radius:2%;
    height:30%;       
    `
const Vote=styled.p
    `
    ${minWidth861(
        `
         font-size:40px;
        `
    )}
    color:green;
    font-weight:bold;
    justify-self:center;
    margin-top:40px;
    height:100%;
    grid-area:vote;
    `
const VoteUp=styled.button
    `
    ${minWidth861(
        `
        justify-self:center;
        font-size:1vmin;
        padding:0 1%;
        margin-top:10%;
        `
    )}
  
    grid-area:voteup;
    background-color:silver;
    `
const VoteDown=styled.button
    `
    ${minWidth861(
        `
        justify-self:center;
        font-size:1vmin;
        padding:0 1%   
        
        `
    )}
    grid-area:votedown;
    background-color:silver;
    `

const Headline=styled.p
    `
    ${minWidth861(
        `
        margin-left:2%;
        word-break:break-all;
        overflow:auto;
        height:80px;
        font-size:20px;
        letter-spacing:1px;
        
        white-space: nowrap;
        overflow: hidden;
        text-overflow: ellipsis;
        text-align:center;
        `
    )}
    &:hover{
        white-space:normal;
        overflow:auto;
        text-overflow:clip;
        ::-webkit-scrollbar {
            width: 0px;
            background: transparent;
        }
        color: #669952
    }
    color: #585858;
    font-family: "Times New Roman", Times, serif;
    font-weight:bold;
    grid-area:headline;
    `
const Author=styled.p
    `
    ${minWidth861(
        `
        margin-left:2%;
        `
    )}
    color:black;
    letter-spacing:2px;
    text-align:center;
    font-weight:bold;
    grid-area:author;
    animation-name:
    `


class ListArticles extends Component{

    constructor(props){
        super(props)
        this.props.initArticles();
    }

    render(){
        return(
            <MainDiv>

                {this.props.articles.map((article,index)=>{
                    return(
                        <Article key={index}>
                            <Vote>{article.numberOfVotes}</Vote>
                            <VoteUp>Vote up</VoteUp>
                            <VoteDown>Vote down</VoteDown>
                            <Headline>{article.headline}</Headline>
                            <Author>{article.author}</Author>
                        </Article>
                    );
                })}

            </MainDiv>
        )
    }
}


function mapStateToProps(store){
    return{
        articles:store.articles.list
    }
}
function mapDispatchToProps(dispatch){

    return{
        initArticles:()=>{dispatch(initGetArticles())}
    }
}


export default connect(mapStateToProps,mapDispatchToProps)(ListArticles)