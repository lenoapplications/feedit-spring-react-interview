
export function compare(query,articles,dispatch){
    const results=[];
    for (let index in articles){

        if (compareQueryWithHeadline(query,articles[index].headline)){
            results.push(articles[index]);
        }
    }
    return results;
}

function compareQueryWithHeadline(query,headline){

    const lettersHeadline=headline.split("");
    const lettersQuery=query.split("");

    for (let i=0; i<lettersHeadline.length; i++){

        if (checkLetterInHeadline(i,lettersQuery,lettersHeadline)){
            return true;
        }
    }
}

function checkLetterInHeadline(index,query,headline){

    for (let q=0,h=index;q<query.length; ++q,++h ){

        if (h >= headline.length){
            return false;
        }
        else if (query[q] !== headline[h]){
            return false;
        }
        ++index;
    }
    return headline[index] === " ";
}



export default{compare}