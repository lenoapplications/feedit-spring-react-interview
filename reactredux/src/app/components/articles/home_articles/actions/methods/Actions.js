import {types} from "../../../../../reducers/articles/Types";


export function initGetArticles_action(list){
    return{
        type:types.initArticles,
        payload:list,
    }

}



export default {initGetArticles_action}