import {apiAxios} from '../../../../../../util/apiCaller/Axios'
import {initGetArticles_action} from "./Actions";

export function initArticles(page,size,sort){
    const config={headers:{"Content-Type":"application/x-www-form-urlencoded"}}

    return apiAxios.get(`/v1/articles/?page=${page}&size=${size}&sort=${sort}`,config)
        .then((resp)=>{
            return resp.data.content;
        })
        .catch((err)=>{
            console.log(err);
        })
}




