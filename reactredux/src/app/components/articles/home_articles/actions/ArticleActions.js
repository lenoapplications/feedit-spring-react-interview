import {compare} from './methods/SearchArticle'
import {initArticles} from "./methods/GetArticles";
import {initGetArticles_action} from "./methods/Actions";

export function searchArticle(event,articles){
    const target=event.target;
    const query=target.parentNode.childNodes[0].value;

    return (dispatch)=>{
        if (query.length === 0){
            //dispatch(searchArticle_noQuery())
        }
        const result=compare(query,articles,dispatch);
        //dispatch(searchArticle_action(result));
    }
}


export function initGetArticles(){

    return (dispatch)=>{
        initArticles(0,2,"headline").then((articles)=>{
            dispatch(initGetArticles_action(articles));
        })
    }
}


export default {initGetArticles}