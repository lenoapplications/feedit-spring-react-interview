export function loginUnsuccess_action(message){
    return{
        type:"USER_LOGIN_UNSUCCESSFUL",
        payload:message,
    }
}

export function loginSucces_action(username) {
    return {
        type: "USER_SUCCESS",
        payload: {
            username: username,
        }
    }
}

export default {loginUnsuccess_action,loginSucces_action}

