import {apiAxios, loginAxios} from "../../../../../../util/apiCaller/Axios";
import jsonMap from "../../../../../../util/tools/JsonMapper";
import QueryString from 'querystring';
import {loginSucces_action,loginUnsuccess_action} from "./Actions";

export function validForm(loginForm){

    const inputs=loginForm.childNodes;
    const username=inputs[1].value;
    const password=inputs[3].value;

    if (username.length > 4 && password.length > 4){
        return jsonMap(["username","password"],[username,password]);

    } else{
        const warnings=warningMessages();

        const warningMessage=( username.length < 4 && password.length < 4 )?
            warnings.userAPass
            :
            ( username.length < 4 )? warnings.user:warnings.pass;

        return loginUnsuccess_action(warningMessage);
    }
}




export function sendLoginRequest(json,dispatch){

    return loginAxios.post("/oauth/token?grant_type=password",QueryString.stringify(json))
        .then((resp)=> { loginSuccess(dispatch,resp,json.username) })
        .catch((err)=>{ loginUnsuccess(dispatch,err) } );
}


function loginSuccess(dispatch,resp,username){
    const token=resp.data.access_token;
    const refToken=resp.data.refresh_token;

    saveTokens(token,refToken);
    setupApiAxios(dispatch,token);

    return dispatch(loginSucces_action(username))
}

function loginUnsuccess(dispatch,err){
    let message;
    message=loginFailedMessages();

    switch(err.response.status) {
        case 400:
            message=message["401"]
            break;
        case 401:
            message = message["401"];
            break;
        case 500:
            message = message["500"];
            break;
    }
    return dispatch(loginUnsuccess_action(message));
}




function setupApiAxios(dispatch,token){
    apiAxios.defaults.headers.common["Authorization"] ='Bearer '+token;
}


function saveTokens(token,refreshToken){
    localStorage.setItem("access_token",token);
    localStorage.setItem("refresh_token",refreshToken);
}



function warningMessages(){
    return{
        userAPass:"Username and password should be at least 5 character long!",
        user:"Username should be at least 5 character long!",
        pass:"Password should be at least 5 character long!"
    }
}

function loginFailedMessages(){
    return{
        500:"Something went wrong on server side, please try again later!",
        401:"Username is not authorized for this application",
    }
}



