
import {sendLoginRequest,validForm} from "./methods/Authorization";

function userLogin(event){
    event.preventDefault();

    return (dispatch)=>{

        let vyalidationResult=validForm(event.target);

        if (typeof validationResult === 'object'){
            return sendLoginRequest(validationResult,dispatch);
        }else{
            return dispatch(validationResult);
        }
    }
}


export default userLogin;