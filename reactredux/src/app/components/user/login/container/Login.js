import React,{Component} from 'react';
import styled from 'styled-components';
import { connect } from 'react-redux';
import {bindActionCreators} from 'redux';
import {gridDisplay} from '../../../../style/GeneralStyle';
import {maxWidth860,minWidth861} from "../../../../style/ResponsiveHandler";
import {animationLoginResponseMessage} from "../../../../style/Animations"

//Actions
import userLogin from '../actions/LoginAction';



//Styled Components
const DivLogin=styled.div
    `
    ${minWidth861(`
        display:grid;
        grid-template-columns:2fr 5fr 2fr;
        grid-template-rows: auto;
        height: 100%;
        grid-template-areas:
                ". form .";
    `)}
    `

const FormLogin=styled.div
    `
    ${minWidth861(`
        grid-area: form;
        display:flex;
        flex-direction: column;
        align-items: center;
        height:100%;
    `)}
    `

const Form=styled.form
    `
    ${minWidth861(`
        display:flex;
        height:40%;
        width:35%;
        padding: 5%;
        flex-direction: column;
        margin:auto;
    `)}
    `

const InputText=styled.input
    `
     font-weight: bold;
    `
const InputSubmit=styled.input
    `
      letter-spacing: 2px;
      margin-top: 7%;
      color:black;
    `

const Label=styled.label
    `
     text-align: center;
     margin-top: 1%;
     letter-spacing: 2px;
    `

const DivLoginStatus=styled.div
    `
      width:50%;
      height:30%;
      text-align: center;
    `

const LoginStatus=styled.p
    `
    ${animationLoginResponseMessage()}
    font-size:2.3vmin;
    font-weight: bold;
    word-wrap: break-word;
    animation-name: responseMessage ;
    animation-duration: 1s;
    animation-iteration-count: infinite;
    `


class Login extends Component{

    constructor(props){
        super(props);
    }

    render(){

        return(
            <DivLogin>
                <FormLogin>
                    <Form onSubmit={this.props.login}>

                        <Label htmlFor="loginUsername">Username: </Label>
                        <InputText type="text" id="loginUsername"/>

                        <Label htmlFor="loginPassword">Password:</Label>
                        <InputText type="password" id="loginPassword"/>

                        <InputSubmit type="submit" value="Login"/>

                    </Form>
                    <DivLoginStatus>
                        <LoginStatus>{this.props.loginStatus}</LoginStatus>
                    </DivLoginStatus>

                </FormLogin>

            </DivLogin>
        )
    }
}


function mapStateToProps(store){
    return{loginStatus:store.infoView.loginRoute.loginStatus}
}
function mapDispatchToProps(dispatch){

    return{
        login:(e)=>{dispatch(userLogin(e))}
    }
}


export default connect(mapStateToProps,mapDispatchToProps)(Login);

