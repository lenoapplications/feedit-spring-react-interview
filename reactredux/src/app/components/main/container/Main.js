import React,{Component} from 'react';
import styled from 'styled-components';
import { connect } from 'react-redux';
import {gridDisplay} from '../../../style/GeneralStyle';
import {maxWidth860,minWidth861} from "../../../style/ResponsiveHandler";
import {animationHeader} from "../../../style/Animations";
import { BrowserRouter } from 'react-router-dom';
import {Route} from 'react-router'

//Component imports
import Login from '../../user/login/container/Login';
import Home from '../../articles/home_articles/container/Home'


//Styled components
const App=styled.div
    `
    ${gridDisplay(`1fr`,`1fr 7fr`)}
    grid-template-areas:
    " header"
    " content";
    height: 100vh; 
    `;


//Header -title,general,username div
const Header=styled.div
    `
    ${minWidth861(`
        display:flex;
    `)}
    width:100%;
    grid-area: header;
    `;
const FeeditDiv=styled.div
    `
    ${minWidth861(`
        display: flex;
        height: 100%;
        background-color: white;
        flex:1;
        overflow:visible;
     `)}
    `;
const FeeditH2=styled.h2
    `
    ${animationHeader()}
    font-weight: bold;
    font-family:SFMono-Regular, Menlo, Monaco, Consolas, "Liberation Mono", "Courier New", monospace;
    letter-spacing: 3px;
    animation-name: animationHeader;
    animation-duration: 2.5s;
    font-size:35px;
    margin-top:1%;
    `

const FeeditH2Span=styled.span
    `
     color:green;
    `

const UsernameDiv=styled.div
    `
    ${minWidth861(`    
        display:flex;
        background-color: #3a3838; 
        clip-path: polygon(0px 0px, 100% 100%, 100% 0px );
        margin-left: auto;
        flex:1;`)}
    `
const UsernameP=styled.p
    `
     ${minWidth861(`
        padding-right: 2%;
        padding-left:2%;
        margin-left: auto;
        margin-right: 3%;
        align-self: center;
        font-family: OpenSymbol;
        font-weight: bold;
        font-size: 3.5vmin;
        letter-spacing: 2px;
        border-radius: 10px;
        border-bottom: #00ce51 inset 2px;
        color:white;
        border-top: #0FA0CE inset 2px;
        transition: border-bottom-width 500ms linear;`)}
     
    `



const Content=styled.div
    `
    margin-top:2%;
    grid-area: content;
    background-color: white;
    
    `;
//----------------------------------------------------------------------------------------------------------------------




class Main extends Component{

    constructor(props){
        super(props);
    }

    render(){
        return(
            <App>
                <Header>
                    <FeeditDiv>
                        <FeeditH2><FeeditH2Span>FEE</FeeditH2Span>DIT</FeeditH2>
                    </FeeditDiv>
                    <UsernameDiv>
                        <UsernameP>{this.props.username}</UsernameP>
                    </UsernameDiv>
                </Header>
                <Content>
                    {this.props.username === undefined ?
                        <Home/>
                        :
                        <Home/>
                    }
                </Content>
            </App>
        );
    }
}


function mapStateToProps(store){
    return{
        username:store.user.username,
    }
}

export default connect(mapStateToProps)(Main)