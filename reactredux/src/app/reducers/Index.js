//Redux
import {combineReducers} from 'redux'


//Reducers

import user from './Users';
import infoView from './InfoView';
import articles from './articles/Articles'


const store=combineReducers({
    user:user,
    infoView:infoView,
    articles:articles,
});



export default store;