import {cases,types} from './Types';

const initialState={
    info:{
        numOfAllArticles:0,
    },
    list:[],

}

export default function(state=initialState,action){

    switch(action.type){

        case types.initArticles:return cases.initArticles(state,action)

        default:return state;
    }
}


