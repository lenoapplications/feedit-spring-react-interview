const initialState={
    loginRoute:{
        loginStatus:undefined
    }
}



export default function infoView(state=initialState, action){

    switch(action.type){
        case "USER_LOGIN_UNSUCCESSFUL":return{...state,loginRoute:{loginStatus:action.payload}};

        default:return state;

    }
}