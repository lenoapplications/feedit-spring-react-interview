const initialState={
    username:undefined,
}

export default function user(state=initialState,action){

    switch(action.type){
        case "USER_SUCCESS":return action.payload;

        default:return state;
    }
}