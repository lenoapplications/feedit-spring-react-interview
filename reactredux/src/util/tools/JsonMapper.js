export default function jsonMapper(keys,values)
{
    if (keys.length === values.length)
    {
        var json={};

        for (var i=0; i<keys.length; i++)
        {
            json[keys[i]]=values[i];
        }
        return json;
    }
    else
    {
        return {"Error":"arguments dont't match"};
    }
}
