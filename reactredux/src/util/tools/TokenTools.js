import jwtDecode from 'jwt-decode'
import jsoMapper from "./JsonMapper";
import QueryString from 'querystring';

export function isTokenExpired(token){
    try{
        const decodedJwt=jwtDecode(token);
        const currentTime=Date.now()/1000;

        return (decodedJwt.exp !== undefined && decodedJwt.exp < currentTime)
    }
    catch (ex){
        return false;
    }
}

export function getNewAccessToken(loginAxios){
    const refreshToken=localStorage.getItem("refresh_token");
    const config={ headers:{"Content-Type":"application/x-www-form-urlencoded"}}
    const json=jsoMapper(["grant_type","refresh_token"],["refresh_token",refreshToken])

    return loginAxios.post("/oauth/token",QueryString.stringify(json),config).then(resp=>{
        localStorage.setItem("access_token",resp.data.access_token);
        localStorage.setItem("refresh_token",resp.data.refresh_token);
        return resp.data.access_token;
    });
}


export default{isTokenExpired,getNewAccessToken}