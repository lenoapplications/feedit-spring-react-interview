import axios from 'axios';
import environment from '../../env/Environment';
import {isTokenExpired,getNewAccessToken} from "../tools/TokenTools";

export const loginAxios=axios.create({
    baseURL:`${environment.api.url}`,
    auth:{
        username:'feeditreact',
        password:'secret'
    },

});

export const apiAxios=axios.create({
    baseURL:`${environment.api.url}`,
})



apiAxios.interceptors.request.use((config)=>{

    const token=localStorage.getItem("access_token");

    if (token !== undefined && isTokenExpired(token)){

        return getNewAccessToken(loginAxios).then((token)=>{
            config.headers["Authorization"]=`Bearer ${token}`
            return Promise.resolve(config);
        })
    }
    return Promise.resolve(config);
});

export default {loginAxios,apiAxios}
