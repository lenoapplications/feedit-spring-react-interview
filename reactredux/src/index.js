import React from 'react';
import ReactDOM from 'react-dom';
import {Provider} from 'react-redux';
import {createStore,applyMiddleware} from 'redux';
import thunk from 'redux-thunk';
import Reducer from './app/reducers/Index';
import { BrowserRouter } from 'react-router-dom';
import {Route} from 'react-router'
import registerServiceWorker from './registerServiceWorker';

//CSS
import Style from './index.css';
import normalizeCss from './lib/sceletonCss/Skeleton-2.0.4/css/normalize.css';
import sceletonCss from './lib/sceletonCss/Skeleton-2.0.4/css/skeleton.css';


//Main component (container)
import Main from './app/components/main/container/Main';


const store=createStore(Reducer,applyMiddleware(thunk));

ReactDOM.render(<Provider store={store}>
                    <BrowserRouter>
                        <Route path="/" component={Main}></Route>
                    </BrowserRouter>
                </Provider>, document.getElementById('root'));

registerServiceWorker();
