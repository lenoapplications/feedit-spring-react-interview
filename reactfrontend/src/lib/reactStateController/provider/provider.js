import React from 'react'

import Component from '../component/generalComponent';


class Provider extends React.Component{

    constructor(props){

        super(props);

        this.state=props.states;
    }

    render_component(arg){

        const component_state=this.get_state(arg.states);

        if (arg.methods === undefined){
            arg.methods={};
        }
        arg.methods.newState=this.change_state.bind(this);
        arg.methods.newStateTimeout=this.change_state_timeout.bind(this);

        return <Component c={arg.constructor}
                          r={arg.render}
                          state={component_state}
                          method={arg.methods}
                          render_component={this.render_component.bind(this)}/>
    }
    render(){

        return(
            <div>
                {this.render_component(this.props.child)}
            </div>
        );
    }
    get_state(states){

        if (states === undefined){
            return {}
        }

        return states(this.state);
    }
    change_state(callback,...args){
        const new_state=callback(this.state,...args);
        this.setState(new_state);
    }

    change_state_timeout(callback,timeout,...args){
        const new_state=callback(this.state,...args);
        setTimeout(()=>{this.setState(new_state)},timeout);
    }
}


export default Provider;