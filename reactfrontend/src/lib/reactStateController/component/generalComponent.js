import React from 'react';



class Component extends React.Component {

    constructor(props) {
        super(props);

        if (props.c) {
            props.c(this);
        }
        this.addMethods(props.method);

        this.render=this.bind_render(props.r);
        this.state=props.state;
        this.addComponent=this.props.render_component;
    }



    componentWillReceiveProps(newProps){
        this.render=this.bind_render(newProps.r);
        this.addMethods(newProps.method);
        this.setState(newProps.state);
    }
    addMethods(methods) {
        for (const key in methods) {
            if (key === 'newState' || key === 'newStateTimeout') {
                this[key] = methods[key];
            }

            else {
                this[key]=methods[key];
            }
        }
    }
    bind_render(render_callback) {
        return render_callback.bind(this);
    }
}




export default Component;
