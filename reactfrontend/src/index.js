import React from 'react';
import ReactDOM from 'react-dom';

//COMPONENTS AND STORE
import Provider from './lib/reactStateController/provider/provider';
import Store from './app/appStore/store';
import App from './app/mainComponent';

//STYLE
import indexStyle from './index.css'
import sceletonCss from './lib/sceletonCss/Skeleton-2.0.4/css/skeleton.css'
import normalizeCss from './lib/sceletonCss/Skeleton-2.0.4/css/normalize.css'

import registerServiceWorker from './registerServiceWorker';

ReactDOM.render(<Provider child={App} states={Store}/>, document.getElementById('root'));

registerServiceWorker();

