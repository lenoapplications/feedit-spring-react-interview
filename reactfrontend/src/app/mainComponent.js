import React from 'react'
import Login from './components/login/js/export'
import PreviewArticles from './components/article/previewArticles/js/export'




function mainRender(){

    const render=(this.state.nav === "login")? Login:PreviewArticles
    const component=this.addComponent(render);

    return(
        <div id="mainComponent">

            <div id="divHeader">
                <div id="polygonHeader">
                    <h2><span>FEE</span>DIT</h2>
                </div>
                <div id="polygonGeneral">
                    <h1>{this.state.header}</h1>
                </div>
                <div id="polygonUsername">
                    <p>{this.state.username}</p>
                </div>
            </div>
            <div id="divContent">
                {component}
            </div>

        </div>
    );
}




export default{
    constructor:undefined,
    render:mainRender,
    states:(states)=>{return {username:states.user.username,header:states.infoMessages.header.message,nav:states.pageController.navigation.current}}
}