import axios from 'axios'
import environment from '../../../env/environment'

let api=axios.create({
    baseURL:`${environment.api.url}`,
});




export default api

/*
function gRequest(route,callback,err){
    const url=`${environment.api.url}/${route}`;

    axios.get(url)
        .then(callback)
        .catch(err)
}

function pRequest(route,callback,err,data){
    const url=`${environment.api.url}/${route}`;

    api.post(url,data)
        .then(callback)
        .catch((err)=>{);
}




export default {
    getRequest:gRequest,
    postRequest:pRequest,
    mapJson:jsonMapper
}

*/