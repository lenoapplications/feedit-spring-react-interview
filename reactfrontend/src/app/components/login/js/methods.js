import axios from '../../../tools/requester/axios'
import jsonMap from '../../../tools/requester/jsonMapper'

function userLogin(e){
    e.preventDefault();

    let data=checkCredentials(this.refs.refLogin);

    if (typeof data === "object"){
        sendCredentials(data,this.newState);
    }else{
        warnUser(data,this.newState);
    }
}

function checkCredentials(loginForm){

    const inputs=loginForm.childNodes;
    const username=inputs[1].value;
    const password=inputs[3].value;

    if (username.length > 4 && password.length > 4){
        return jsonMap(["username","password"],[username,password]);
    }else{
        const warnings=warningMessages();

        return( username.length < 4 && password.length < 4 )?
            warnings.userAPass
              :
            ( username.length < 4 )? warnings.user:warnings.pass;
    }
}

function sendCredentials(json,newState){

    const loginSuccess=(states)=>{
        states.user.username=json.username;
        states.pageController.navigation.current="previewArticles";
        return states;
    }
    const loginFailed=(states,err)=>{
        let message;
        const failMessages=loginFailedMessages();

        switch(err.response.status){
            case 401:message=failMessages["401"];break;
            case 500:message=failMessages["500"];break;
        }
        states.infoMessages.loginComponent.loginStatus=message;
        return states;
    }
    axios.post("/login",json)
         .then(()=> { newState(loginSuccess) } )
         .catch((err)=>{ newState(loginFailed,err) } )
}


function warnUser(warning,newState){

    const callback=(states)=>{
        states.infoMessages.loginComponent.loginStatus=warning
        return states;
    }
    newState(callback);
}




function warningMessages(){
    return{
        userAPass:"Username and password should be at least 5 character long!",
        user:"Username should be at least 5 character long!",
        pass:"Password should be at least 5 character long!"
    }
}

function loginFailedMessages(){
    return{
        500:"Something went wrong on server side, please try again later!",
        401:"Username is not authorized for this application",
    }
}


export default{
    login:userLogin,
}