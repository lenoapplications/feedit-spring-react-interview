import Render from './render'
import Methods from './methods'



export default {

    constructor:undefined,
    render:Render,
    methods:Methods,
    states:(states)=> {
        return {
            headerMessage:states.infoMessages.header.message,
            loginStatus:states.infoMessages.loginComponent.loginStatus,
            nav:states.pageController.navigation.current
        }
    }

}

