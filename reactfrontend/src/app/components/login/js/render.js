import React from 'react'

//Style
import mobileStyle from '../css/mobileStyleLogin.css'
import pcStyle from '../css/pcStyleLogin.css'

export default function render(){
    return(
        <div id="mainLogin" >

            <div id="loginForm">
                <form ref="refLogin">
                    <label htmlFor="loginUsername">USERNAME:</label>
                    <input type="text" id="loginUsername"/>

                    <label htmlFor="loginPassword">PASSWORD:</label>
                    <input type="password" id="loginPassword"/>

                    <input type="submit" onClick={this.login.bind(this)} value="Submit"/>
                </form>

                <div id="loginStatus">
                    <p>{this.state.loginStatus}</p>
                </div>
            </div>
        </div>
    );
}