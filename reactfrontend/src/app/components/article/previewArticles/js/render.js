import React from 'react';

//Style
import mobileStyle from '../css/mobileStylePreviewArticle.css'
import pcStyle from '../css/pcStylePreviewArticle.css'



export default function render(){

    let component= welcomeAnimation.bind(this)();
    return(
        <div>
            {component}
        </div>
    );
}



//welcome animation
function welcomeAnimation(){

    switch(this.state.animationPhase){
        case "phaseOne":
            return previewArticlePhaseOne.bind(this)();

        case "phaseTwo":
            return previewArticlePhaseTwo.bind(this)();

    }
}









//Phase one animation
function previewArticlePhaseOne(){
    this.animationPhaseSwitch("phaseTwo",600);
    return(
        <div id="mainPreviewArticles_phaseOne">
            {articles.bind(this)()}
        </div>
    )
}
//Phase two animation
function previewArticlePhaseTwo(){
    return(
        <div id="mainPreviewArticles_phaseTwo">
            {articles.bind(this)()}
            {preview.bind(this)()}
        </div>
    )
}


//Components
function articles(){
    return (
        <div id="previewArticles_articles">
            {this.state.articles.map((article,index)=>{
                    return (
                        <div key={index} className="previewArticles_article">
                            <p id="article_vote">{article.vote}</p>
                            <button id="article_voteup">vote up</button>
                            <button id="article_votedown">vote down</button>
                            <p id="article_headline">{article.headline}</p>
                            <p id="article_author">{article.author}</p>
                        </div>
                    );
                })
            }
        </div>
    );
}


function preview(){
    return(
      <div id="previewArticles_events">
            <button>CREATE ARTICLE</button>
          <select name="sort"id="articles_event">
          </select>
      </div>
    );
}
