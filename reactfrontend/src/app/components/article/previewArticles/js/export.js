import Render from './render'
import Methods from './methods'


export default {
    constructor:undefined,
    render:Render,
    methods:Methods,
    states:(states)=> {
        return {
            headerMessage:states.infoMessages.header.message,
            username:states.user.username,
            animationPhase:states.pageController.signals.previewArticle.animationPhase,
            articles:states.articles.list,
        }
    }
}

