export default {

    navigation:{
        current:"login"
    },
    pageController:{
        navigation:{
            current:"login"
        },
        signals:{
            previewArticle:{
                animationPhase:"phaseOne"
            }
        }
    },
    infoMessages:{

        header:{
            message:"WELCOME"
        },

        loginComponent:{
            loginStatus:undefined
        }
    },
    user:{
        username:undefined
    },

    articles:{
        list:[{vote:0,headline:"Steve jobs mindset conquer world",link:"http://www.google.hr",author:"LeNo"},
              {vote:0,headline:"Bill Gates mindset",link:"http://www.google.hr",author:"LeNo"}]
    }
}